#!bin/bash
if [ "$1" == 'help' ];then
    echo "Usage: $0 tag(optional) releaseVersion(optional) developmentVersion(optional)"
    echo "examples: "
    echo "$0"
    echo "$0 vis-1.1.25 1.1.25 1.1.26-SNAPSHOT"
    echo "$0 vis-1.1.25"
    echo "$0 vis-1.1.25 1.1.25"
    exit 0
fi
#项目目录
JDEPLOY_HOME=`dirname $(cd "$(dirname "$0")"; pwd)`
#构建工程名称
PROJECT_NAME=AppDemo
#构建目录
BUILD_HOME=$JDEPLOY_HOME/data/build
#SVN资源库地质
SVN_URL=https://scm.bswan.domain:18080/svn/qim/AppDemo/trunk

#CHECKOUT项目
BUILD_PROJECT_PATH=$BUILD_HOME/$PROJECT_NAME
rm -rf $BUILD_PROJECT_PATH
svn checkout $SVN_URL $BUILD_PROJECT_PATH
cd $BUILD_PROJECT_PATH

TAG_VERSION=$1
RELEASE_VERSION=$2
DEV_VERSION=$3
MVN_RELEASE_COMMAND="mvn -B release:prepare "
if [ "$TAG_VERSION" != "" ];then
    MVN_RELEASE_COMMAND="${MVN_RELEASE_COMMAND} -Dtag=${TAG_VERSION}"
fi
if [ "$RELEASE_VERSION" != "" ];then
    MVN_RELEASE_COMMAND="${MVN_RELEASE_COMMAND} -DreleaseVersion=${RELEASE_VERSION}"
fi
if [ "$DEV_VERSION" != "" ];then
    MVN_RELEASE_COMMAND="${MVN_RELEASE_COMMAND} -DdevelopmentVersion=${DEV_VERSION}"
fi
echo ${MVN_RELEASE_COMMAND}
eval ${MVN_RELEASE_COMMAND}
##这里只想release:perform命令
#mvn release:perform