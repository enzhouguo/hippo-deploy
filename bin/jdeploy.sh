#!/bin/sh
if [ $# -lt 1 ];then
    echo "Usage: $0 tagName profile(optional)"
    exit 1
fi
#项目目录
JDEPLOY_HOME=`dirname $(cd "$(dirname "$0")"; pwd)`

#构建工程名称
PROJECT_NAME=AppDemo
#构建目录
BUILD_HOME=$JDEPLOY_HOME/data/build
#部署目录
DEPLOY_HOME="/data/java"
#项目版本
SVN_TAG=$1
#目标IP
IP=10.121.49.131
#SVN资源库地址
SVN_PRO=https://scm.qiyi.domain:18080/svn/qim/AppDemo/tags/$PROJECT_NAME-${SVN_TAG}
#SVN_PRO=http://10.1.20.56/svn/pps_monitor/trunk/qim-web

#CHEK OUT项目
BUILD_PROJECT_PATH=$BUILD_HOME/$PROJECT_NAME
rm -rf $BUILD_PROJECT_PATH
svn checkout $SVN_PRO $BUILD_PROJECT_PATH
cd $BUILD_PROJECT_PATH
MAVEN_PROFILE=${2:-test}
echo $MAVEN_PROFILE
echo Maven构建$PROJECT_HOME项目
mvn clean package -Dmaven.test.skip=true -P ${MAVEN_PROFILE}
#远程部署
DEPLOY_PROJECT_PATH=$DEPLOY_HOME/$PROJECT_NAME
ssh root@$IP "rm -rf $DEPLOY_PROJECT_PATH/* && mkdir -p $DEPLOY_PROJECT_PATH"
scp $BUILD_PROJECT_PATH/target/$PROJECT_NAME.war root@${IP}:$DEPLOY_PROJECT_PATH/$PROJECT_NAME.war	
ssh root@${IP} "mkdir -p $DEPLOY_PROJECT_PATH; cd $DEPLOY_PROJECT_PATH; unzip $PROJECT_NAME.war && rm -f $PROJECT_NAME.war"
ssh root@${IP} "/usr/local/tomcat/bin/restart.sh"
 
##循环拷贝制品到服务器上面
#for server in `cat ${AB_BASE_DIR}/conf/AppDemo-${MAVEN_PROFILE}`
#do
#        echo "scp build artifacts to remote server: $server"
#        ssh root@${server} "mkdir -p /data/build/"
#        scp AppDemo.war root@${server}:/data/build/
#        echo "scp build artifacts complete..."
#done
 
##循环部署远程服务并启动
#for server in `cat ${AB_BASE_DIR}/conf/AppDemo-${MAVEN_PROFILE}`
#do
#        echo "start remote service..."
#        ##通过ssh执行指定的项目安装部署命令
#        ssh root@${server} "mkdir -p /data/java/AppDemo; cd /data/java/AppDemo; unzip AppDemo.war && rm -f AppDemo.war"
#        ssh root@${server} "/usr/local/tomcat/bin/restart.sh"
#done


