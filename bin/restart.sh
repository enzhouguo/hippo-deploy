#!/bin/sh
TOMCAT_PATH=`dirname "$0"`

echo "TOMCAT_PATH is $TOMCAT_PATH"

PID=`ps aux | grep tomca[t] | grep jav[a] | awk '{print $2}'`

if [ -n "$PID" ]; then
        echo "Will kill tomcat: $PID"
        sh "$TOMCAT_PATH/shutdown.sh"
        sleep 6
else echo "No Tomcat Process $PID"
fi

PID=`ps aux | grep tomca[t] | grep jav[a] | awk '{print $2}'`

while [ -n "$PID" ]; do
        kill  $PID && sleep 1
        echo "Try to kill $PID"
        PID=`ps aux | grep tomca[t] | grep jav[a] | awk '{print $2}'`
done
#test -n $PID && kill $PID && sleep 1

sh "$TOMCAT_PATH/startup.sh"
sleep 3

PID=`ps aux | grep tomca[t] | grep jav[a] | awk '{print $2}'`
if [ -n "$PID" ]; then
        echo "\nRestart tomcat successfully!"
else
        echo "\nFail to startup tomcat"
        exit 1
fi
